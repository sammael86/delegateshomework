﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesHomework
{
    public static class Finder
    {
        public static void FinderSample()
        {
            Console.WriteLine("Поиск файлов в заданной директории (нажмите пробел для прерывания):");
            FileFinder fileFinder = new FileFinder(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            fileFinder.FileFound += OnFileFound;

            var tokenSource = new CancellationTokenSource();
            var task = fileFinder.FindFiles(tokenSource.Token);

            while (!task.IsCompleted)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.Spacebar)
                {
                    Console.WriteLine("Поиск файлов прерван пользователем.");
                    tokenSource.Cancel();
                    break;
                }
            }

            fileFinder.FileFound -= OnFileFound;
            tokenSource.Dispose();
        }

        static void OnFileFound(object sender, FileArgs e)
        {
            Console.WriteLine(e.FileName);
        }
    }
}
