﻿using System;

namespace DelegatesHomework
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; private set; }

        public FileArgs(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException(nameof(fileName));

            FileName = fileName;
        }
    }
}
