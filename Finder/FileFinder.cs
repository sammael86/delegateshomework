﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesHomework
{
    public class FileFinder
    {
        private string folderPath;

        public FileFinder(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentNullException(nameof(path));

            if(!Directory.Exists(path))
                throw new DirectoryNotFoundException(path);

            folderPath = path;
        }

        public event EventHandler<FileArgs> FileFound;

        public async Task FindFiles(CancellationToken token)
        {
            foreach (var fileName in Directory.EnumerateFiles(folderPath))
            {
                FileFound?.Invoke(this, new FileArgs(fileName));
                
                // Эмуляция длительности процесса поиска
                await Task.Delay(250);

                if (token.IsCancellationRequested)
                    break;
            }

            if (!token.IsCancellationRequested)
                Console.WriteLine("Поиск файлов успешно завершён. Нажмите любую клавишу для выхода.");
        }
    }
}
