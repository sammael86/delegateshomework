﻿using System;
using System.Collections.Generic;

namespace DelegatesHomework
{
    public static class GetMax
    {
        public static void GetMaxSample()
        {
            Console.WriteLine("Поиск самого взрослого пользователя...");
            List<Account> accounts = new List<Account>
            {
                new Account {Id = 1, Age = 28, Name = "Alex"},
                new Account {Id = 2, Age = 26, Name = "Oleg"},
                new Account {Id = 3, Age = 30, Name = "Andrew"},
                new Account {Id = 4, Age = 25, Name = "Ivan"}
            };
            accounts.GetMax(getAccountAge).PrintAccount();
            Console.WriteLine();

            Console.WriteLine("Поиск самой длинной строки...");
            List<string> strings = new List<string>
            {
                "sdfkljsf",
                "dsfgdfgdgh",
                "dfgsdf",
                "dsfgsdfgdsdsfg",
                "sdfg"
            };
            Console.WriteLine(strings.GetMax(getStringLength));
            Console.WriteLine();
        }

        static float getAccountAge(Account account)
        {
            return account.Age;
        }

        static float getStringLength(string str)
        {
            return str.Length;
        }
    }
}
