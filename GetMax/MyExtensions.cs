﻿using System;
using System.Collections.Generic;

namespace DelegatesHomework
{
    public static class MyExtensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T instance = default;
            float maxValue = 0f;
            foreach (var element in e)
            {
                float tempValue = getParameter(element);
                if (tempValue > maxValue)
                {
                    maxValue = tempValue;
                    instance = element;
                }
            }
            return instance;
        }
    }
}
