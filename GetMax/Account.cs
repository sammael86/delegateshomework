﻿using System;

namespace DelegatesHomework
{
    public class Account
    {
        public int Id { get; set; }
        public float Age { get; set; }
        public string Name { get; set; }

        public void PrintAccount()
        {
            Console.WriteLine($"ID: {Id}, Age: {Age}, Name: {Name}");
        }
    }
}
